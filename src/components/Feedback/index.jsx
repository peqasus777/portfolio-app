/*eslint-disable */
import styled from "styled-components";
import { Link } from "react-router-dom";

const Container = styled.div`
  width: 95%;
  display: flex;
  flex-direction: column;
  gap: 10px;
  margin-bottom: 20px;
`;

// const Triangle = styled.div`
//   position: absolute;
//   bottom: 0;
//   left: 0;
//   transform: translateX(-50%);
//   width: 0;
//   height: 0;
//   border-left: 15px solid transparent;
//   border-right: 15px solid transparent;
//   border-bottom: 15px solid #eee; 
//   z-index: 1;
// `;

const Text = styled.div`
  /* position: relative; */
  width: 100%;
  font-family: Open Sans;
  font-size: 14px;
  font-weight: 400;
  line-height: 21px;
  letter-spacing: 0em;
  text-align: left;
  padding: 15px;
  background: #eee;
  color: var(--text-color);
`;

const User = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  gap: 30px;
`;

const Img = styled.div`
  max-width: 50px;
  max-height: 50px;
  border-radius: 50%;
  border: 1px solid purple;
  img {
    border-radius: 50%;
    width: 100%;
    height: 100%;
  }
`;

const P = styled.p`
  font-family: Open Sans;
  font-size: 12px;
  font-weight: 400;
  line-height: 14px;
  letter-spacing: 0em;
  text-align: left;
  color: var(--text-color);
  a {
    color: var(--title-color);
  }
`;

export const Feedback = ({ text, image, name, link, siteName }) => {
  return (
    <Container>
      <Text>{text}
      {/* <Triangle /> */}
      </Text>
      <User>
        <Img>
          <img src={image} alt="avatar" />
        </Img>
        <P>
          {name}
          <Link to={link}>{siteName}</Link>
        </P>
      </User>
    </Container>
  );
};
