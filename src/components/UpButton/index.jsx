import styled from "styled-components";
import { FaAngleUp } from "react-icons/fa";

const Button = styled.div`
    width: 35px;
    height: 35px;
    background-color: var(--text-color);
    position: fixed;
    right: 20px;
    bottom: 0;
    color: #fff;
    font-size: 20px;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    z-index: 1;
    &:hover{
      background-color:#2b333f;
    }
`

export const UpButton = () => {
  const handleUpButtonClick = () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  return (
    <Button  onClick={handleUpButtonClick}>
      <FaAngleUp />
    </Button>
  );
};
