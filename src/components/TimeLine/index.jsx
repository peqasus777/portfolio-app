/*eslint-disable */
import styled from "styled-components";

const Container = styled.div`
  width: 97%;
  display: flex;
  gap: 10px;
  margin-bottom: 20px;
`;
const Date = styled.div`
  position: relative;
  width: 10%;
  display: flex;
  flex-direction: column;
  gap: 20px;
  hr {
    position: absolute;
    top: 70px;
    left: -30px;
    border: none;
    background: var(--title-color);
    rotate: calc(90deg);
    width: 120px;
    height: 6px;
    border-radius: 5px;
    @media only screen and (max-width: 768px) {
      top: 100px;
      left: -55px;
      width: 200px;
      height: 6px;
      margin-left: -25px;
      margin-top: 40px;
    }
    @media only screen and (max-width: 450px) {
      top: 190px;
      left: -125px;
      width: 340px;
      height: 6px;
      margin-left: -25px;
      margin-top: 40px;
      z-index: 1;
      /* overflow-y: scroll; */
    }
  }
`;
// const Triangle = styled.div`
//   position: absolute;
//   top: 20px;
//   left: 0;
//   transform: translateX(-50%);
//   rotate: calc(45deg);
//   width: 0;
//   height: 0;
//   border-right: 15px solid transparent;
//   border-bottom: 15px solid #eee;

// `;

const Text = styled.div`
  position: relative;
  width: 90%;
  font-family: Open Sans;
  font-size: 14px;
  font-weight: 400;
  line-height: 21px;
  letter-spacing: 0em;
  text-align: left;
  padding: 15px;
  background: #eee;
  color: var(--text-color);
  p {
    font-family: Open Sans;
    font-size: 16px;
    font-weight: 700;
    line-height: 19px;
    letter-spacing: 0em;
    text-align: left;
    margin-bottom: 10px;
  }
`;
export const TimeLine = ({ date, school, desc }) => {
  return (
    <>
      <Container>
        <Date>
          <p>{date}</p>
          <hr />
        </Date>
        <Text>
          <p>{school}</p>
          {desc}
          {/* <Triangle /> */}
        </Text>
      </Container>
    </>
  );
};
