import styled from "styled-components";

const LineWithText = styled.div`
  flex-grow: 1;
  height: 0.2px;
  background: #667081;
  margin: 0 3px;
`;
const Text = styled.div`
  color: #667081;
  margin-bottom: 1px;
  font-family: Open Sans;
  font-size: 13px;
  font-weight: 400;
  display: flex;
  justify-content: space-between;
  margin-top: 10px;
`;
const PipeLine = styled.div`
  color: #667081;
  margin-bottom: 1px;
  font-family: Open Sans;
  font-size: 13px;
  font-weight: 400;
  display: flex;
  justify-content: space-between;
`;

export const SkillLine = () => {
  return (
    <>
      <PipeLine>
        <p>|</p>
        <p>|</p>
        <p>|</p>
        <p>|</p>
      </PipeLine>
      <LineWithText />
      <Text>
        <p>Beginner</p>
        <p> Proficient</p>
        <p> Expert</p>
        <p> Master</p>
      </Text>
    </>
  );
};