import { useRef } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import {
  FaAngleRight,
  FaAngleLeft,
  FaUser,
  FaChartBar,
  FaBriefcase,
  FaCode,
  FaImages,
  FaAddressCard,
  FaComment,
  FaBars,
} from 'react-icons/fa';

import logo from '../../assets/images/zamir.jpg';
import { UpButton } from '../UpButton';

const ICON_SIZE = '20px';

const Nav = styled.nav`
  position: absolute;
  top: 0;
  left: 0;
  position: fixed;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 15dvh;
  min-height: 100vh;
  background-color: var(--title-color);
  padding: 0 1rem 0 1rem;
  transition: transform 1s;

  &.navbar {
    transform: translateX(-100%);
  }

  @media only screen and (max-width: 768px) {
    span {
      display: none;
    }
    h1 {
      display: none;
    }
    width: 50px;
    padding: 10px;
  }

  h1 {
    color: #fff;
    font-size: 19px;
    font-weight: 700;
    margin-top: -55px;
  }
`;

const NavBtn = styled.button`
  position: absolute;
  transform: translateX(38px);
  top: 20px;
  right: 0;
  width: 40px;
  height: 60px;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: var(--title-color);
  outline: none;
  border: none;
  font-size: 1rem;
  padding: 5px 10px;
  cursor: pointer;
  color: #fff;

  @media only screen and (max-width: 768px) {
    position: absolute;
    transform: translateX(38px);
    top: 20px;
    right: 20px;
    width: 30px;
    height: 40px;
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
    display: flex;
    justify-content: center;
    align-items: center;
    /* background-color: #753ffd; */
    outline: none;
    border: none;
    font-size: 1rem;
    padding: 5px 10px;
    cursor: pointer;
    color: #fff;
  }
`;

const Logo = styled(Link)`
  display: block;
  width: 100px;
  padding: 25px;
  background: transparent;

  @media only screen and (max-width: 768px) {
    width: 40px;
    padding: 10px 0;
  }

  img {
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 50%;
  }
`;

const LinkStyled = styled(Link)`
  display: flex;
  align-items: center;
  color: #fff;
  text-decoration: none;
  padding: 10px 1rem;
  border-radius: 5px;
  margin-bottom: 5px;

  &:hover {
    background-color: #8fe4b7;
    color: #000000;
  }

  &.active {
    background-color: #8fe4b7;
    color: #000000;
  }

  span {
    margin-left: 10px;
  }
`;

const MobileNav = styled.div`
  background-color: #753ffd;
  width: 100%;
  height: 40px;
  display: none;
`;

const MobileNavBtn = styled.button`
  color: #fff;
  background: transparent;
  outline: none;
  border: none;
  margin: 0 10px;
`;

// eslint-disable-next-line react/prop-types
export function Navigation({ visible, show }) {
  const activeLinkRef = useRef(null);

  const handleNavLinkClick = (e, targetId) => {
    
    e.preventDefault();
    const targetSection = document.getElementById(targetId);
    if (targetSection) {
      window.scrollTo({
        top: targetSection.offsetTop,
        behavior: 'smooth',
      });
    }

    if (activeLinkRef.current) {
      activeLinkRef.current.classList.remove('active');
    }

    e.target.classList.add('active');

    activeLinkRef.current = e.target;
  };

  return (
    <>
      <MobileNav>
        <MobileNavBtn onClick={() => show(!visible)}>
          <FaBars size={24} />
        </MobileNavBtn>
      </MobileNav>
      <Nav className={!visible ? 'navbar' : ''}>
        <NavBtn type="button" onClick={() => show(!visible)}>
          {!visible ? <FaAngleRight size={30} /> : <FaAngleLeft size={30} />}
        </NavBtn>
        <Logo to="/">
          <img src={logo} alt="logo" />
        </Logo>
        <h1>Zamir Novruz</h1>
        <div className="links nav-top">
          <LinkStyled
            to="#"
            onClick={(e) => handleNavLinkClick(e, 'about')}
            className="link"
          >
            <FaUser size={ICON_SIZE} />
            <span>About Me</span>
          </LinkStyled>

          <LinkStyled
            to="#"
            onClick={(e) => handleNavLinkClick(e, 'educations')}
            className="link"
          >
            <FaChartBar size={ICON_SIZE} />
            <span>Educations</span>
          </LinkStyled>
          <LinkStyled
            to="#"
            onClick={(e) => handleNavLinkClick(e, 'experience')}
            className="link"
          >
            <FaBriefcase size={ICON_SIZE} />
            <span>Experience</span>
          </LinkStyled>
          <LinkStyled
            to="#"
            onClick={(e) => handleNavLinkClick(e, 'skills')}
            className="link"
          >
            <FaCode size={ICON_SIZE} />
            <span>Skills</span>
          </LinkStyled>
          <LinkStyled
            to="#"
            onClick={(e) => handleNavLinkClick(e, 'portfolio')}
            className="link"
          >
            <FaImages size={ICON_SIZE} />
            <span>Portfolio</span>
          </LinkStyled>
          <LinkStyled
            to="#"
            onClick={(e) => handleNavLinkClick(e, 'contacts')}
            className="link"
          >
            <FaAddressCard size={ICON_SIZE} />
            <span>Contacts</span>
          </LinkStyled>
          <LinkStyled
            to="#"
            onClick={(e) => handleNavLinkClick(e, 'feedback')}
            className="link"
          >
            <FaComment size={ICON_SIZE} />
            <span>Feedback</span>
          </LinkStyled>
        </div>
        <div className="links">
          <LinkStyled to="/">
            <FaAngleLeft size={ICON_SIZE} />
            <span>Go Back</span>
          </LinkStyled>
        </div>
      </Nav>
      <UpButton 
      />

    </>
  );
}
