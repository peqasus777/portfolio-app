import styled from 'styled-components';
import {TimeLine, Title} from '../../../components'

const educationsData = [
  {
    date: "2022-2023",
    school: "Epam UpSkill",
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore cum magni cupiditate dolorum minima, et ex! Aut praesentium necessitatibus ipsam eligendi similique reiciendis! Quo ipsa itaque et mollitia enim harum magnam quidem, iure sed atque laudantium eum quae distinctio. Odio explicabo laudantium ex sed sunt non numquam consectetur, in eligendi tempora pariatur ipsa aliquid eaque possimus est nostrum minus qui, rerum labore ab voluptatibus earum perspiciatis inventore modi. Tenetur voluptatum asperiores quam deserunt nemo temporibus neque magnam veritatis sit sunt aliquam, consectetur esse iusto, necessitatibus omnis odit ipsum perferendis. Officia iure omnis possimus voluptas odit nostrum, dolorem minus provident delectus!",
  },
  {
    date: "2017-2022",
    school: "IATC Developer",
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore cum magni cupiditate dolorum minima, et ex! Aut praesentium necessitatibus ipsam eligendi similique reiciendis! Quo ipsa itaque et mollitia enim harum magnam quidem, iure sed atque laudantium eum quae distinctio. Odio explicabo laudantium ex sed sunt non numquam consectetur, in eligendi tempora pariatur ipsa aliquid eaque possimus est nostrum minus qui, rerum labore ab voluptatibus earum perspiciatis inventore modi. Tenetur voluptatum asperiores quam deserunt nemo temporibus neque magnam veritatis sit sunt aliquam, consectetur esse iusto, necessitatibus omnis odit ipsum perferendis. Officia iure omnis possimus voluptas odit nostrum, dolorem minus provident delectus!",
  },
  {
    date: "2013-2017",
    school: "Sales Manager",
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore cum magni cupiditate dolorum minima, et ex! Aut praesentium necessitatibus ipsam eligendi similique reiciendis! Quo ipsa itaque et mollitia enim harum magnam quidem, iure sed atque laudantium eum quae distinctio. Odio explicabo laudantium ex sed sunt non numquam consectetur, in eligendi tempora pariatur ipsa aliquid eaque possimus est nostrum minus qui, rerum labore ab voluptatibus earum perspiciatis inventore modi. Tenetur voluptatum asperiores quam deserunt nemo temporibus neque magnam veritatis sit sunt aliquam, consectetur esse iusto, necessitatibus omnis odit ipsum perferendis. Officia iure omnis possimus voluptas odit nostrum, dolorem minus provident delectus!",
  },
  {
    date: "2010-2013",
    school: "Galant MMC",
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore cum magni cupiditate dolorum minima, et ex! Aut praesentium necessitatibus ipsam eligendi similique reiciendis! Quo ipsa itaque et mollitia enim harum magnam quidem, iure sed atque laudantium eum quae distinctio. Odio explicabo laudantium ex sed sunt non numquam consectetur, in eligendi tempora pariatur ipsa aliquid eaque possimus est nostrum minus qui, rerum labore ab voluptatibus earum perspiciatis inventore modi. Tenetur voluptatum asperiores quam deserunt nemo temporibus neque magnam veritatis sit sunt aliquam, consectetur esse iusto, necessitatibus omnis odit ipsum perferendis. Officia iure omnis possimus voluptas odit nostrum, dolorem minus provident delectus!",
  },
  {
    date: "2006-2010",
    school: "Parametric MMC",
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore cum magni cupiditate dolorum minima, et ex! Aut praesentium necessitatibus ipsam eligendi similique reiciendis! Quo ipsa itaque et mollitia enim harum magnam quidem, iure sed atque laudantium eum quae distinctio. Odio explicabo laudantium ex sed sunt non numquam consectetur, in eligendi tempora pariatur ipsa aliquid eaque possimus est nostrum minus qui, rerum labore ab voluptatibus earum perspiciatis inventore modi. Tenetur voluptatum asperiores quam deserunt nemo temporibus neque magnam veritatis sit sunt aliquam, consectetur esse iusto, necessitatibus omnis odit ipsum perferendis. Officia iure omnis possimus voluptas odit nostrum, dolorem minus provident delectus!",
  },
  {
    date: "2002-2006",
    school: "Caspian Spyard ",
    desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore cum magni cupiditate dolorum minima, et ex! Aut praesentium necessitatibus ipsam eligendi similique reiciendis! Quo ipsa itaque et mollitia enim harum magnam quidem, iure sed atque laudantium eum quae distinctio. Odio explicabo laudantium ex sed sunt non numquam consectetur, in eligendi tempora pariatur ipsa aliquid eaque possimus est nostrum minus qui, rerum labore ab voluptatibus earum perspiciatis inventore modi. Tenetur voluptatum asperiores quam deserunt nemo temporibus neque magnam veritatis sit sunt aliquam, consectetur esse iusto, necessitatibus omnis odit ipsum perferendis. Officia iure omnis possimus voluptas odit nostrum, dolorem minus provident delectus!",
  },
];


const Container =styled.div`
width: 100%;
height: 50vh;
overflow-y: scroll;
cursor: pointer;
`

export const Educations = () => {
  return (
    <div id='educations'>
      <Title title="Educations"/>
<Container>
{educationsData.map((education, index) => (
        <TimeLine key={index} date={education.date} school={education.school} desc={education.desc} />
      ))}
</Container>
    </div>
  );
};

