import { RangeSkill, SkillLine, Title } from "../../../components"

export const Skill = () => {
  return (
    <div id="skills">
    <Title title="Skills"/>
    <RangeSkill text="HTML" value="100"/>
    <br />
    <RangeSkill text="CSS" value="95"/>
    <br />
    <RangeSkill text="JavaScript" value="85"/>
    <br />
    <RangeSkill text="ReactJS" value="80"/>
    <br />
    <RangeSkill text="Node.Js" value="75"/>
    <br />
    <RangeSkill text="Express.Js" value="65"/>
    <br />
    <SkillLine/>
    
    </div>

  )
}
