import { Title, Feedback } from "../../../components";
import Avatar from "../../../assets/images/user.jpg";

export const Feedbacks = () => {
  return (
    <div id="feedback">
      <Title title="Feedback" />
      <Feedback
        text="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quas dolorum numquam deleniti, omnis dignissimos consectetur voluptates ullam fuga eligendi dicta officiis recusandae harum dolorem soluta magni eum! Sequi consequatur, numquam rerum officiis dolor saepe deleniti quaerat, asperiores optio reiciendis vitae id natus quisquam perspiciatis beatae voluptatem. Magnam officia accusamus suscipit debitis totam? Tenetur ullam consequatur laboriosam aut natus tempore beatae possimus, fugit repudiandae ducimus, illo veritatis aliquid libero excepturi consequuntur obcaecati tempora, omnis assumenda quod dicta voluptatem. Numquam, quaerat velit! Beatae facilis sequi officia maxime, quod vero obcaecati? Corporis ducimus deserunt sunt quo delectus vel maiores veritatis harum nulla sint"
        image={Avatar}
        name="Zamir Novruz- Front-end Developer, "
       
      />
      <Feedback
        text="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quas dolorum numquam deleniti, omnis dignissimos consectetur voluptates ullam fuga eligendi dicta officiis recusandae harum dolorem soluta magni eum! Sequi consequatur, numquam rerum officiis dolor saepe deleniti quaerat, asperiores optio reiciendis vitae id natus quisquam perspiciatis beatae voluptatem. Magnam officia accusamus suscipit debitis totam? Tenetur ullam consequatur laboriosam aut natus tempore beatae possimus, fugit repudiandae ducimus, illo veritatis aliquid libero excepturi consequuntur obcaecati tempora, omnis assumenda quod dicta voluptatem. Numquam, quaerat velit! Beatae facilis sequi officia maxime, quod vero obcaecati? Corporis ducimus deserunt sunt quo delectus vel maiores veritatis harum nulla sint"
        image={Avatar}
        name="Zamir Novruz - Front-end Developer, "
       
      />
         <Feedback
        text="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quas dolorum numquam deleniti, omnis dignissimos consectetur voluptates ullam fuga eligendi dicta officiis recusandae harum dolorem soluta magni eum! Sequi consequatur, numquam rerum officiis dolor saepe deleniti quaerat, asperiores optio reiciendis vitae id natus quisquam perspiciatis beatae voluptatem. Magnam officia accusamus suscipit debitis totam? Tenetur ullam consequatur laboriosam aut natus tempore beatae possimus, fugit repudiandae ducimus, illo veritatis aliquid libero excepturi consequuntur obcaecati tempora, omnis assumenda quod dicta voluptatem. Numquam, quaerat velit! Beatae facilis sequi officia maxime, quod vero obcaecati? Corporis ducimus deserunt sunt quo delectus vel maiores veritatis harum nulla sint"
        image={Avatar}
        name="Zamir Novruz - Front-end Developer, "
        
      />

    </div>
  );
};
