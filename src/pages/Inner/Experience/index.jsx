import {Expertise, Title} from '../../../components'


export const Experience = () => {
  return (
    <div id='experience'>
    <Title title="Experience"/>
      <Expertise company="Google" date="2019-2022" job="Front-end developer / php programmer" 
      description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
          commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus
          et magnis dis parturient montes, nascetur ridiculus mus. Donec quam
          felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla
          consequat massa quis enim. Donec pede justo, fringil"/>
      <Expertise company="Twitter" date="2017-2019" job="Web Developer" 
      description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
          commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus
          et magnis dis parturient montes, nascetur ridiculus mus. Donec quam
          felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla
          consequat massa quis enim. Donec pede justo, fringil"/>
    </div>
  )
}
