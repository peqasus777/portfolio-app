import { FaPhoneAlt, FaSkype, FaTwitter, FaFacebookF } from "react-icons/fa";
import { GrMail } from "react-icons/gr";
import { Address, Title } from '../../../components';

export const Contact = () => {
  return (
    <div id="contacts">
      <Title title="Contacts" />
      <Address toLink="tel:+994553054721" icon={<FaPhoneAlt />} text="+994 55 305 47 21" />
      <Address toLink="mailto:peqasus777@gmail.com" icon={<GrMail />} text="peqasus777@gmail.com" />
      <Address toLink="https://join.skype.com/invite/HvbEpWScOYg0" icon={<FaSkype />} text="Skype" />
      <Address toLink="https://www.facebook.com/peqasus777/" icon={<FaFacebookF />} text="Facebook" />
      <Address toLink="https://twitter.com/peqasus777" icon={<FaTwitter />} text="Twitter" />
    </div>
  );
};
