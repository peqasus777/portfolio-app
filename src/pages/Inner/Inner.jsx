import { useState } from "react";
import { Navigation } from "../../components";

import {
  About,Contact,Educations,Experience,Feedbacks,Portfolio,Skill
} from "../Inner";

import "./inner.css";

const Inner = () => {
  const [navVisible, showNavbar] = useState(true);

  return (
    <div className="container">
      <Navigation visible={navVisible} show={showNavbar} />
      <div className={!navVisible ? "page" : "page page-with-navbar"}>
        <About />
        <Educations />
        <Experience />
        <Skill />
        <Portfolio />
        <Contact />
        <Feedbacks />
      </div>
    </div>
  );
};

export default Inner;